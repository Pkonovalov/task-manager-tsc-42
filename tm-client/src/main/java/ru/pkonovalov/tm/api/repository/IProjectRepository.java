package ru.pkonovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.endpoint.Project;

public interface IProjectRepository extends IRepository<Project> {

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    String getIdByName(@NotNull String userId, @NotNull String name);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

}
