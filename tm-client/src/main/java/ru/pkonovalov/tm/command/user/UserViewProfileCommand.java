package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show user profile";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-view";
    }

    @Override
    public void execute() {
        @Nullable final User user = endpointLocator.getUserEndpoint().viewUser(endpointLocator.getSession());
        if (user == null) return;
        showUser(user);
    }

}
