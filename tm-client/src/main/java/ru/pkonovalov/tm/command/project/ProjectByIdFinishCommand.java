package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.entity.NoProjectsException;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish project by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-finish-by-id";
    }

    @Override
    public void execute() {
        if (endpointLocator.getProjectEndpoint().countProject(endpointLocator.getSession()) < 1)
            throw new NoProjectsException();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        endpointLocator.getProjectEndpoint().finishProjectById(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
