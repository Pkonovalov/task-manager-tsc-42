package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;
import ru.pkonovalov.tm.endpoint.Project;
import ru.pkonovalov.tm.endpoint.Role;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;

import static ru.pkonovalov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] commandRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        if (project.getStatus() != null)
            System.out.println("STATUS: " + project.getStatus());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("STARTED: " + project.getDateStart());
        System.out.println("FINISHED: " + project.getDateFinish());
        System.out.print(dashedLine());
    }

}
