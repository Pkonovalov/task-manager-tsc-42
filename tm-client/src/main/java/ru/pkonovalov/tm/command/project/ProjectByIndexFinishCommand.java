package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByIndexFinishCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish project by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-finish-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        endpointLocator.getProjectEndpoint().finishProjectByIndex(endpointLocator.getSession(), TerminalUtil.nextNumber());
    }

}
