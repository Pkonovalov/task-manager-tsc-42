package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Status;
import ru.pkonovalov.tm.exception.entity.TaskNotFoundException;
import ru.pkonovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskByIdSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Set task status by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-set-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!endpointLocator.getTaskEndpoint().existsTaskById(endpointLocator.getSession(), id))
            throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        endpointLocator.getTaskEndpoint().setTaskStatusById(endpointLocator.getSession(), id, Status.fromValue(TerminalUtil.nextLine()));
    }

}
