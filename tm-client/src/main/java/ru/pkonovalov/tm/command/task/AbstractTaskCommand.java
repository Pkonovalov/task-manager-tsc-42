package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;
import ru.pkonovalov.tm.endpoint.Role;
import ru.pkonovalov.tm.endpoint.Task;
import ru.pkonovalov.tm.exception.entity.TaskNotFoundException;

import static ru.pkonovalov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] commandRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        if (task.getStatus() != null)
            System.out.println("STATUS: " + task.getStatus());
        System.out.println("PROJECT ID: " + task.getProjectId());
        System.out.println("CREATED: " + task.getCreated());
        System.out.println("STARTED: " + task.getDateStart());
        System.out.println("FINISHED: " + task.getDateFinish());
        System.out.print(dashedLine());
    }

}
