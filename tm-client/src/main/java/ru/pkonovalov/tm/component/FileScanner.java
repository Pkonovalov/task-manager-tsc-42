package ru.pkonovalov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.bootstrap.Bootstrap;
import ru.pkonovalov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private final static String FILE_DIRECTORY = "./";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private final int interval;

    public FileScanner(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.interval = propertyService.getScannerInterval();
    }

    public void init() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getArgsCommands()) {
            commands.add(command.commandName());
        }
        es.scheduleWithFixedDelay(this, 0, interval, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(FILE_DIRECTORY);
        for (@NotNull final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            bootstrap.parseCommand(fileName);
            item.delete();
        }
    }

    public void stop() {
        es.shutdown();
    }

}
