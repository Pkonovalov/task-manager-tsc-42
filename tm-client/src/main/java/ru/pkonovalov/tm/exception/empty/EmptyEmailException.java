package ru.pkonovalov.tm.exception.empty;

import ru.pkonovalov.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! E-mail is empty...");
    }

}
