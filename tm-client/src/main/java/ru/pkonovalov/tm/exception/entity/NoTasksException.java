package ru.pkonovalov.tm.exception.entity;

public class NoTasksException extends RuntimeException {

    public NoTasksException() {
        super("Error! No tasks...");
    }

}
