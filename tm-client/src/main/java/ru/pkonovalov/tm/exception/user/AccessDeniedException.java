package ru.pkonovalov.tm.exception.user;

import ru.pkonovalov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
