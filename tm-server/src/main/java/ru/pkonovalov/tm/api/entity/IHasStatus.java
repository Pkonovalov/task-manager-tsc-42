package ru.pkonovalov.tm.api.entity;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
