package ru.pkonovalov.tm.api;

import ru.pkonovalov.tm.dto.AbstractBusinessEntityDTO;

public interface IService<E extends AbstractBusinessEntityDTO> extends IRepository<E> {

}
