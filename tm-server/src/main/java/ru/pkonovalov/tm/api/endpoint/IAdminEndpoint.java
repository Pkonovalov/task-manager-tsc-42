package ru.pkonovalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @WebMethod
    void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @WebMethod
    void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @WebMethod
    void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

}
