package ru.pkonovalov.tm.endpoint;

import ru.pkonovalov.tm.api.service.ServiceLocator;

public class AbstractEndpoint {

    protected final ServiceLocator serviceLocator;

    AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
